Rails.application.routes.draw do
  get 'welcome/dipesh'

  root 'welcome#index'

  resources :articles do
    resources :comments
  end

  get '/dipesh/:id',to: 'welcome#dipu'
  get '/dipesh/:id/:name',to: 'welcome#mukesh'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
